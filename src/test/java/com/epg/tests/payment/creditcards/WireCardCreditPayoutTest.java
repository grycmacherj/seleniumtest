package com.epg.tests.payment.creditcards;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WireCardCreditPayoutTest extends AbstratWireCard {
//todo all
	@Test
	public void test() throws InterruptedException {
		auth("payin");
		checkout("4012000300001003","003");
		callback();
		loginStagingBo();
		boTransactionSearch();

		List<WebElement> wireCard = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentSolution' and text()='WireCard']"));
		List<WebElement> amount = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentAmountCurrency' and text()='66 (EUR)']"));
		List<WebElement> status = driver
				.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='SUCCESS']"));
		List<WebElement> operationDebit = driver
				.findElements(By.xpath("//td[@id='transactionSummaryOperation']/span[text()='DEBIT']"));
		
		Assert.assertEquals(1, wireCard.size());
		Assert.assertEquals(1, amount.size());
		Assert.assertEquals(1, operationDebit.size());
		Assert.assertEquals(1, status.size());
		
	}

}
