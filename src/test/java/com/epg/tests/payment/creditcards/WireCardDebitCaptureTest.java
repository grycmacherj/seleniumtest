package com.epg.tests.payment.creditcards;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WireCardDebitCaptureTest extends AbstratWireCard {

	@Test
	public void testCapture() throws InterruptedException {
		
		auth("auth");
		checkout("4200000000000000", "123");
		callback();
		loginStagingBo();
		boTransactionSearch();
		
		List<WebElement> wireCard = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentSolution' and text()='WireCard']"));
		List<WebElement> amount = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentAmountCurrency' and text()='66 (EUR)']"));
		List<WebElement> status = driver
				.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='PENDING']"));

		Assert.assertEquals(1, wireCard.size());
		Assert.assertEquals(1, amount.size());
		Assert.assertEquals(1, status.size());
		
		captureOperation();
		
	}

	private void captureOperation() throws InterruptedException {
		
		click("captureBtn");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		click("//a[@class='flavr-button success captureBtn' and text()='Capture']");

		WebElement successCapture = findIdOrNameOrXpath("//div[@class='alert alert-success']//p[contains(text(),'Success Capture')]");
		Assert.assertTrue((successCapture.getText()).contains("Success Capture"));

		Thread.sleep(5000);

		click("//body[@id='tdetails']//div[@class='flavr-overlay']");

		Thread.sleep(5000);

		click("//body[@id='tdetails']/button[@id='refreshTD']");

		List<WebElement> statusFinal = driver.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='SUCCESS']"));
		Assert.assertEquals(1, statusFinal.size());
		
	}
	
}
