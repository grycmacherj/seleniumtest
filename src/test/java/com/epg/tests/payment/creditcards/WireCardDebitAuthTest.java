package com.epg.tests.payment.creditcards;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WireCardDebitAuthTest extends AbstratWireCard {

	@Test
	public void testAuth() throws InterruptedException {
		
		auth("auth");
		checkout("4200000000000000", "123");
		callback();
		loginStagingBo();
		boTransactionSearch();
		
		List<WebElement> wireCard = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentSolution' and text()='WireCard']"));
		List<WebElement> amount = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentAmountCurrency' and text()='66 (EUR)']"));
		List<WebElement> status = driver
				.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='PENDING']"));

		Assert.assertEquals(1, wireCard.size());
		Assert.assertEquals(1, amount.size());
		Assert.assertEquals(1, status.size());
		
	}

}
