package com.epg.tests.payment.creditcards;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WireCardDebitVoidTest extends AbstratWireCard {

	@Test
	public void testVoid() throws InterruptedException {
		
		auth("auth");
		checkout("4200000000000000", "123");
		callback();
		loginStagingBo();
		boTransactionSearch();
		
		List<WebElement> wireCard = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentSolution' and text()='WireCard']"));
		List<WebElement> amount = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentAmountCurrency' and text()='66 (EUR)']"));
		List<WebElement> status = driver
				.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='PENDING']"));
		List<WebElement> operationDebit = driver
				.findElements(By.xpath("//td[@id='transactionSummaryOperation']/span[text()='DEBIT']"));

		Assert.assertEquals(1, wireCard.size());
		Assert.assertEquals(1, amount.size());
		Assert.assertEquals(1, operationDebit.size());
		Assert.assertEquals(1, status.size());
		
		operationVoid();
		
	}

	private void operationVoid() throws InterruptedException {
		
		click("voidBtn");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		click("//a[@class='flavr-button danger voidBtn' and text()='Void']");

		WebElement successVoid = findIdOrNameOrXpath("//div[@class='alert alert-success']//p[contains(text(),'Success reversal')]");
		Assert.assertTrue((successVoid.getText()).contains("Success reversal"));
		
		Thread.sleep(5000);
		
		click("//body[@id='tdetails']//div[@class='flavr-overlay']");

		Thread.sleep(5000);

		click("//body[@id='tdetails']/button[@id='refreshTD']");

		List<WebElement> statusFinal = driver.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='PENDING']"));
		Assert.assertEquals(1, statusFinal.size());
		
		WebElement last2ndOperation = driver.findElement(By.xpath("//td[@id='lastSecOperation' and text()='VOID']"));
		Assert.assertTrue((last2ndOperation.toString()).contains("VOID"));

	}

}
