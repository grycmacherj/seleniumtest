package com.epg.tests.payment.creditcards;

import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class WireCardDebitRefundTest extends AbstratWireCard {

	@Test
	public void test() throws InterruptedException {
		auth("payin");
		checkout("4200000000000000", "123");
		callback();
		loginStagingBo();
		boTransactionSearch();
		
		List<WebElement> wireCard = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentSolution' and text()='WireCard']"));
		List<WebElement> amount = driver
				.findElements(By.xpath("//td[@id='transactionSummaryPaymentAmountCurrency' and text()='66 (EUR)']"));
		List<WebElement> statusSuccess = driver
				.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='SUCCESS']"));

		Assert.assertEquals(1, statusSuccess.size());
		Assert.assertEquals(1, wireCard.size());
		Assert.assertEquals(1, amount.size());
		
		refund();//check it
	}

	private void refund() throws InterruptedException {
		click("refundBtn");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		click("//a[@class='flavr-button success refundBtn' and text()='Refund']");

		WebElement successRefound = findIdOrNameOrXpath(
				"//div[@class='alert alert-success']//p[@class='flavrP' and text()='Success Rebate']");
		Assert.assertTrue((successRefound.toString()).contains("Success Rebate"));

		// press refresh button and check last 2ndary operation
		
		Thread.sleep(5000);
		
		click("//body[@id='tdetails']//div[@class='flavr-overlay']");
		
		Thread.sleep(5000);

		click("//body[@id='tdetails']/button[@id='refreshTD']");

		List<WebElement> statusFinal = driver.findElements(By.xpath("//td[@id='transactionSummaryStatus']/span[text()='SUCCESS']"));
		Assert.assertEquals(1, statusFinal.size());

		WebElement last2ndOperation = driver.findElement(By.xpath("//td[@id='lastSecOperation' and text()='REFUND']"));
		Assert.assertTrue((last2ndOperation.toString()).contains("REFUND"));
	}

}
