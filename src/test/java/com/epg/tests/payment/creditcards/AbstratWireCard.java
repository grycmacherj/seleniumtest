package com.epg.tests.payment.creditcards;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.epg.AbstractEpgFramework;

public abstract class AbstratWireCard extends AbstractEpgFramework {

	private static final String URL_MERCHANT_SIMULATOR = "http://test.easypaymentgateway.com/MerchantSimulator/tokenize?";
	private static final String BO_LOGIN = "EPG1115admin";
	private static final String BO_PASSWORD = "Te$t1115";
	private static final String URL_STAGING_BO = "http://staging.easypaymentgateway.com/PortalWeb/";
	// private static final String URL_BO_SEARCH =
	// "http://staging.easypaymentgateway.com/PortalWeb/#transactionSearch";

	protected String customerId;

	protected void auth(String lastName) throws InterruptedException {

		// Go to merchant simulator and fill in the form, submit
		open(URL_MERCHANT_SIMULATOR);
		Thread.sleep(15000);

		epgSelectInCombo("Local", "Staging.EPG - Checkout");
		epgSelectInCombo("Guts", "Big Testing Merchant");

		setText("paymentSolution", "creditcards");
		setText("operationType", "debit");
		setText("currency", "EUR");
		setText("amount", "66");
		setText("country", "ES");
		setText("firstName", "114");
		setText("lastName", lastName);
		setText("language", "EN");

		customerId = "JGx" + System.currentTimeMillis();
		setText("customerId", customerId);

		click("submitButton");
		Thread.sleep(10000);
	}

	protected void checkout(String cc, String cvn) throws InterruptedException {
		
		// Switch to new window opened: Checkout page
		openWindow("Checkout");
		Thread.sleep(5000);

		// Fill in Checkout form and pay
		findIdOrNameOrXpath("//div/img[@class='button-paysol button-paysol-CreditCards']").click();

		navigateToFrame("CreditCardsFrame");

		setText("cardNumber", cc);
		setText("cvnNumber", cvn);

		click("ccForm");

		epgSelectInCombo("February", "January (01)", false);
		epgSelectInCombo("2016", "2019", false);

		setText("chName", "jg");

		epgSelectInCombo("United Kingdom", "Spain", false);
		Thread.sleep(5000);

		click("//form[@id='ccForm']/button[@class='btn btn-inverse btn-hg btn-block' and @type='submit']");

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	
	protected void callback() {

		// Switch to new window opened: success page in callback
		openWindow("SUCCESS page");

		String url = driver.getCurrentUrl();
		Assert.assertTrue(url.contains("success"));

	}

	protected void loginStagingBo() throws InterruptedException {

		open(URL_STAGING_BO);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		setText("j_username", BO_LOGIN);
		setText("j_password", BO_PASSWORD);
		click("//button[@type='button']");
		Thread.sleep(10000L);

	}

	protected void boTransactionSearch() throws InterruptedException {

		click("//li/a[contains(span/text(),'Monitoring')]");
		Thread.sleep(7000L);

		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath("//li[contains(a//span/text(),'Transaction Search')]"));
		action.moveToElement(we).click().perform();

		Thread.sleep(10000L);

		WebElement transactioncriteria = driver.findElement(By.id("transactioncriteria"));
		Select select = new Select(transactioncriteria);
		select.selectByValue("customer_id");
		setText("transactionSearchDDId", customerId);

		click("searchGo");

		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		sleep(10000L);

		// navigateToFrame("iframe"); doesn't work compare with below
		WebElement iframe = driver.findElement(By.tagName("iframe"));
		driver.switchTo().frame(iframe);

	}

}
