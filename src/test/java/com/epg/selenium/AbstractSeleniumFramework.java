package com.epg.selenium;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AbstractSeleniumFramework {
	
protected WebDriver driver;
	
	@Before
	public void setup() {
		driver = new FirefoxDriver();
	}
	
	protected void open(String url) {
		driver.get(url);
	}
	
	protected void navigateToFrame(String idOrNameOrXpath) {
		WebElement el = findIdOrNameOrXpath(idOrNameOrXpath);
		driver.switchTo().frame(el);
	}
	
	protected void openWindow(String partOfTitle) {
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
			if (driver.getTitle().contains(partOfTitle)) {
				return;
			}
		}
		throw new IllegalArgumentException("Did not find window with partial title: " + partOfTitle);
	}
	
	protected void click(String idOrNameOrXpath) {
		WebElement el = findIdOrNameOrXpath(idOrNameOrXpath);
		el.click();
	}
	
	protected void setText(String idOrNameOrXpath, String text) {
		WebElement el = findIdOrNameOrXpath(idOrNameOrXpath);
		el.clear();
		el.sendKeys(text);
	}
	
	protected void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			Assert.fail(e.getMessage());
		}
	}
	
	protected void changeImplicitWait(long millis) {
		driver.manage().timeouts().implicitlyWait(millis, TimeUnit.MILLISECONDS);
	}

	protected WebElement findIdOrNameOrXpath(String idOrNameOrXpath) {
		
		boolean startsWithSlash = idOrNameOrXpath.startsWith("/");
		WebElement el;
		if (startsWithSlash) {
			el = driver.findElement(By.xpath(idOrNameOrXpath));
		} else {
			List<WebElement> elementsById = driver.findElements(By.id(idOrNameOrXpath));
			if (0 != elementsById.size()) {
				el = elementsById.get(0);
			} else {
				List<WebElement> elementsByName = driver.findElements(By.name(idOrNameOrXpath));
				if (0 != elementsByName.size()) {
					el = elementsByName.get(0);
				} else {
					el = driver.findElement(By.xpath(idOrNameOrXpath));
				}
			}
		}
		return el;
	}

}
