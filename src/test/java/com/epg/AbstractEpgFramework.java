package com.epg;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.epg.selenium.AbstractSeleniumFramework;

public class AbstractEpgFramework extends AbstractSeleniumFramework {
	
	protected void epgSelectInCombo(String currentLabel, String newLabel) {
		epgSelectInCombo(currentLabel, newLabel, true);
	}
	
	protected void epgSelectInCombo(String currentLabel, String newLabel, boolean typeInText) {
		driver.findElement(By.xpath("//button[contains(@title,'"+currentLabel+"')]")).click();
		WebElement servName = driver.findElement(By.xpath("//button[contains(@title,'"+currentLabel+"')]/following-sibling::div"));
		if (typeInText) {
			servName.findElement(By.tagName("input")).sendKeys(newLabel);
		}
		servName.findElement(By.xpath("ul/li/a[contains(span/text(),'"+newLabel+"')]")).click();
	}

}
